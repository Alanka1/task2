insert into Authorization1
(username, email, password, role, forgotPasswordKey, forgotPasswordKeyTimestamp, companyUnitId)
values
('alala', 'alala@gmail.com', '123', 'director', 'ghbdtn', 241423431, 1),
('ololo', 'ololo@gmail.com', '321', 'idiot', 're', 134114, 2),
('elele', 'elele@gmail.com', '231', 'lolik', 'rfr', 1434141247946, 2),
('ilili', 'ilili@gmail.com', '132', 'phantazer', 'ltkf', 1341, 3),
('ululu', 'ululu@gmail.com', '323', 'admin', 'rtr', 476494025, 5);

insert into Fond (fondnumber, createdtimestamp, createdby, updatedtimestamp, updatedby)
values
('38', 73412541913, 34, 79338996, 4817),
('65', 2914537267, 437, 997052231, 87),
('42', 20787502356, 63, 261676563, 4),
('971', 86827801550, 51, 5989935742, 164),
('73', 006514357655, 54, 811346499, 24);

insert into Company (nameRu, nameKz, nameEn, bin, parentId, fondId,createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('a2', 'b1', 'c1', '3432265', 15, 1, 82347652, 78, 034146, 8),
('a3', 'b2', 'c2', '9787633', 754, 2, 9315319443, 06347, 7203934497, 00109),
('a4', 'b3', 'c3', '6227087459', 4803, 4, 3121864165, 64987, 7322436296, 07),
('a5', 'b4', 'c4', '6896568232', 59334, 2, 5816502522, 9, 0077316495, 40008),
('a6', 'b5', 'c5', '3832349960', 765, 3, 6070108108, 821, 7057417748, 29);

insert into TempFiles (fileBinary, fileBinaryByte)
values
('work', 2),
('cry', 1),
('stress', 2),
('suicide', 3),
('java', 3);

insert into SearchKeyRouting (searchKeyId, tableName, tableId, type)
values
(1, 'no', 4, 'docx'),
(2, 'plz', 1, 'txt'),
(4, 'no', 3, 'mp4'),
(3, 'god', 2, 'pdf'),
(5, 'damn', 5, 'pptx');

insert into SearchKey (name, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('oh', 3, 543626224, 72, 4054531687, 62),
('my', 5,13413412, 56, 984532125, 87),
('god', 1, 9087564381, 52, 36453129696, 89),
('ugh', 4, 9413541735, 73,9846576, 50),
('wow', 2, 114357006543, 89, 46598649, 87);


insert into FileRouting (fileId, tableName, tableId, type)
values
(2, '1.com', 2, 'mp3'),
(1, '2.gov', 1, 'gif'),
(2, '3.com', 4, 'jpeg'),
(3, '4.ru', 3, 'excel'),
(4, '5.com', 1, 'powerpoint');

insert into File (name, type, size, pageCount, hash, isDeleted, fileBinaryId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('w', '1.com', 338, 57, '4816075100', false, 5, 9020509454, 342, 4952295117, 194),
('h', '2.com', 275, 72, '5873179530', true, 4, 1899861564, 146, 0209444622, 327),
('y', '3.com', 224, 79, '1819188779', false, 5, 0239404289, 378, 9079889598, 167),
('s', '4.com', 391, 68, '5149949957', true, 3, 4886990673, 496, 6546854694, 240),
('o', '5.com', 146, 78, '6578476415', true, 2, 2229103121, 461, 5549780771, 209);


insert into CompanyUnit(nameRu, nameKz, nameEn, parentId, year, companyId, codeIndex , createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('x1', 'y1', 'z1', 01083, 2011, 1, '497612439', 09798664557, 87, 078976513, 88443),
('x2', 'y2', 'z2', 97892, 2010, 2, '751651581', 14984661, 69748, 873135, 01),
('x3', 'y3', 'z3', 4, 2004, 3, '28451208', 5864513546, 16, 6454125, 62),
('x4', 'y4', 'z4', 7772, 2009, 4, '09874556', 63986415313, 15193, 19876450262, 2974),
('x5', 'y5', 'z5', 81684559, 1996, 3, '187454120', 937846513210, 3782, 48284576, 4728);


insert into Users (authId, name, fullName, surname, secondName, status, companyUnitId, password, lastLoginTimestamp, iin, isActive, isActivated, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
(1, 'durak', 'durachok', 'Pupkin', 'daa', 'single', 1, '1212', 135225, '01120999525', true, false, 6894969345, 3, 2301688666, 678),
(3, 'krasavchik', 'krasavec', 'Lord', 'kraa', 'married', 1, '131313', 14341435, '001107436765', false, true, 0945131, 77,67643, 4);

insert into Share (requestId, note, senderId, receiverId, shareTimestamp)
values
(1, 're', 9770, 5, 7774833498),
(5, 'ra', 9264, 00668, 4216055834),
(1, 'ro', 3202, 07395, 8786300369),
(4, 'ru', 1214, 62, 3551267510),
(3, 'ri', 21, 4, 5120132324);


insert into NomenclatureSummary (number, year, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('7229078237', 1999, 4, 2447093519, 61, 0734096968, 291),
('5016151555', 2012, 3, 2882234570, 181, 8786326031, 469),
('9379928572', 2020, 2, 2927582963, 196, 3386279953, 96),
('6588005806', 2005, 5, 3753255319, 491, 2821078102, 53),
('6771967557', 1969, 2, 0863852602, 456, 0943525934, 144);


insert into DestructionAct (actNumber, base, structuralSubdivisionId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('1813846249', 'hard', 1, 8670199246, 199, 3187088015, 489),
('2741537390', 'to', 2, 1807450309, 417, 8066022336, 425),
('9524650829', 'tell', 4, 7546061067, 165, 9469572793, 404),
('5912585817', 'for', 3, 6428183080, 284, 7694662875, 198),
('2699788102', 'real', 3, 8309233558, 167, 6641862720, 103);

insert into Record (number, type, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('3214137425', 'gif', 3, 7186557295, 101, 7995109857, 68),
('4750781785', 'jpeg', 4, 6173988291, 106, 7257952566, 385),
('9762338429', 'excel', 1, 3481613253, 10, 4663997406, 285),
('6547407345', 'jpeg', 1, 1068739819, 422, 2156529906, 95),
('2468453070', 'jpeg', 4, 5925621743, 106, 9478761048, 349);


insert into Request (requestUserId, responseUserId, caseId, caseIndexId, createdType, comment, status, timestamp, shareStart, shareFinish, favorite, updateTimestamp, updateBy, declineNote, companyUnitId, fromRequestId)
values
(0830, 19012, 1, 1, 'powerpoint', 'Programmable', 'Manager', 1452722382, 9672376189, 7336123252, true, 6119007938, 7617, 'transitional', 4, 470),
(5876, 12, 3, 2, 'word', 'Programmable', 'Engineer', 7362602073, 8312946519, 3445370656, false, 1274613434, 152, 'architected', 2, 4),
(01, 71, 3, 5, 'excel', 'Programmable', 'Manager', 5353367006, 9029527684, 0042500753, false, 3861284367, 16, 'configurable', 1, 90722),
(63, 941, 2, 1, 'powerpoint', 'Programmable', 'Engineer', 5506348193, 8041907679, 1295682419, true, 5415283579, 9, 'devolved', 4, 70488),
(1, 0, 4, 5, 'excel', 'Programmable', 'Manager', 5820344391, 4627341350, 3372218379, true, 2792027223, '13', 'focused', 5, 5);

insert into HistoryRequestStatus (requestId, status, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
(2, 'Programmer', 6611973265, 4, 6478229726, 849),
(2, 'Admin', 3405006325, 896, 3722093198, 63),
(3, 'Programmer', 3692676417, 1, 3160168735, 155),
(4, 'Programmer', 4547100454, 707, 7911551294, 64298),
(5, 'Assistant', 2144694818, 45321, 5957032069, 0365);

insert into CaseIndex (caseIndex, titleRu, titleKz, titleEn, storageType, storageYear, note, companyUnitId, nomenclatureId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('8532207553', 's1.com', 'd1.com', 'f1.com', 1, 1994, 'light', 5, 5, 6197861496, 434, 9131696589, 347),
('4567193977', 's2.com', 'd2.com', 'f2.com', 10, 2005, 'reft', 5, 1, 4718439256, 415, 8232752173, 63),
('6789738412', 's3.com', 'd3.com', 'f3.com', 10, 2006, 'left', 3, 5, 0175360944, 500, 5350385046, 155),
('1234357056', 's4.com', 'd4.com', 'f4.com', 8, 2001, 'right', 1, 1, 9258406036, 327, 8806658255, 112),
('4567333720', 's5.com', 'd5.com', 'f5.com', 9, 1988, 'righteft', 1, 2, 4399937890, 486, 9838229288, 331);


insert into CatalogCase
(caseId, catalogId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
(1, 1, 1, 2201380198, 84, 8566888553, 0523),
(5, 2, 4, 8496285022, 57855, 3410992510, 72),
(1, 1, 3, 2326039238, 712, 0624547787, 51910),
(4, 2, 1, 4610140543, 56, 5590264057, 7),
(2, 4, 3, 9812785965, 131, 4906762638, 1);


insert into Nomenclature ( nomenclatureNumber, year, nomenclatureSummaryId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('9624889634', 1996, 94, 1, 8598626635, 269, 4604420572, 52),
('8605295259', 2004, 56, 1, 6851143607, 187, 1722930667, 246),
('9097005724', 2006, 37, 3, 5684227287, 320, 1037542304, 177),
('7402541991', 1996, 84, 2, 0896820629, 73, 9108726612, 274),
('7320642021', 2010, 45, 4, 6636896329, 217, 2472592167, 259);

insert into Catalog (nameRu, nameKz, nameEn, parentId, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('q1', 'w1', 'e1', 123, 3, 5844186877, 5, 4648543459, 15),
('q2', 'w2', 'e2', 4534, 3, 7145925060, 5375, 4184862446, 03),
('q3', 'w3', 'e3', 4452, 4, 0239391012, 42, 5898211098, 29280),
('q4', 'w4', 'e4', 963, 1, 3901644989, 6, 8148536155, 3133),
('q5', 'w5', 'e5', 7514, 2, 0799330183, 0769, 5719596, 254);

insert into Case1 (caseNumber, caseTom, caseHeadingRu, caseHeadingKz, caseHeadingEn, startDate, finishDate, pageNumber, Eds, edsSignature, sendingNaf, deletionSign, limitedAccess, hash, version, idVersion, activeVersion , note, locationId, caseIndexId, inventoryId, destructionActId, structuralSubdivisionId, caseBlockchainAddress, addBlockchainDate, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('1254', '5', 'Trio', 'Fanyt', 'Ruden', 7208136637, 8416929947, 54, true, 'system engine', true, true, false, '9614678194', 3, '1922526517', true, 'radical', 5, 2, 5, 2, 1, '1298011582', 4447034565, 8953011566, 459, 4342696568, 264),
('657', '7', 'Duo', 'Jope', 'Retyp', 6359001918, 6609508682, 4, false, 'switchable', true, true, false, '7079148030', 3, '4512398368', true, 'Optional', 3, 1, 5, 4, 1, '1523830700', 3657869727, 8277979067, 345, 7700162654, 400),
('899', '7', 'Fure', 'Ferar', 'Drive', 5167225795, 2801527912, 1, false, 'extended', false, true, false, '4473371859', 3, '1538538318', false, 'demand-driven', 2, 4, 1, 4, 2, '7461956663', 7283969284, 9855507495, 482, 4222601702, 187),
('901', '3340', 'Boureu', 'Labek', 'Yandx', 9929634045, 4368955080, 4, false, 'collaboration', false, false, true, '2278910817', 4, '6890575369', true, 'content-based', 5, 1, 2, 4, 4, '3916169793', 7620885110, 0924311754, 176, 0559912293, 322),
('5', '0', 'Depo', 'Lood', 'Yardrey', 7767382540, 1178631192, 13, false, 'extended', false, true, true, '6616340333', 2, '2838479199', false, 'analyzing', 1, 3, 4, 1, 2, '7930925889', 9803425188, 6941085444, 417, 4721344856, 247);

insert into ActivityJournal (eventType, objectType, objectId, createdTimestamp, createdBy, messageLevel, message)
values
('haha', 'hah', 2, 5902979142, 312, '5', 'ryrhfdhdf'),
('hehe', 'heh', 4, 3413127403, 192, '32', 'dhdghdfh'),
('hihi', 'hih', 5, 8734576789, 259, '38', 'dfhyktyjt'),
('huhu', 'huh', 5, 2321147806, 283, '366', 'gfgsdfgd'),
('hoho', 'hoh', 2, 6656280528, 21, '93', 'uouyrrj');

insert into Notification (objectType, objectId, companyUnitId, userId, createdTimestamp, viewedTimestamp, isViewed, title, text, companyId)
values
('maximized', 4, 5, 4, 0662027620, 3298449997, true, 'dret', 'ehhrrehh', 5),
('total', 5, 3, 4, 3360675401, 6732439995, true, 'pret', 'iuytt', 4),
('data-warehouse', 4, 4, 1, 6936108695, 9359950726, false, 'danet', 'poiufg', 5),
('algorithm', 5, 1, 3, 1470860880, 0416548962, true, 'pinet', 'qweqr', 1),
('methodology', 1, 4, 1, 2236985207, 8765432835, false, 'cement', 'kghoeji', 4);


insert into Location (row, line, columnN, box, companyUnitId, createdTimestamp, createdBy, updatedTimestamp, updatedBy)
values
('2374', '909', '42', '61734', 3, 3433588740, 292, 0504843249, 223),
('730', '2665', '56', '982', 3, 4504626325, 329, 7704121529, 420),
('22', '21', '64', '606', 2, 7278760925, 314, 3538321264, 394),
('73', '77', '8', '53', 4, 0100884040, 205, 4843354344, 225),
('90', '35', '15', '1', 2, 0757911021, 100, 5342612160, 99);
