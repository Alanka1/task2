package com.example.crud.controller;

import com.example.crud.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondController {
    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping("/demo/fonds")
    public ResponseEntity<?> getFonds() {
        return ResponseEntity.ok(fondService.getAll());
    }

    @GetMapping("/demo/fond/{id}")
    public ResponseEntity<?> findFondByID(@PathVariable long id){
        return ResponseEntity.ok(fondService.findByID(id));
    }

    @DeleteMapping("/demo/deleteFond/{id}")
    public void deleteFondByID(@PathVariable long id){
        fondService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateFond/{id}/{fondNumber}", method = RequestMethod.GET)
    public void updateFondByID(@PathVariable("id") long id, @PathVariable("fondNumber") String fondNumber){
        fondService.updateByID(id, fondNumber);
    }
}
