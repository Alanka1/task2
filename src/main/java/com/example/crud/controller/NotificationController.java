package com.example.crud.controller;

import com.example.crud.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("/demo/notifications")
    public ResponseEntity<?> getNotifications() {
        return ResponseEntity.ok(notificationService.getAll());
    }

    @GetMapping("/demo/notification/{id}")
    public ResponseEntity<?> findNotificationByID(@PathVariable long id){
        return ResponseEntity.ok(notificationService.findByID(id));
    }

    @DeleteMapping("/demo/deleteNotification/{id}")
    public void deleteNotificationByID(@PathVariable long id){
        notificationService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateNotification/{id}/{objectType}", method = RequestMethod.GET)
    public void updateNotificationByID(@PathVariable("id") long id, @PathVariable("objectType") String objectType){
        notificationService.updateByID(id, objectType);
    }
}
