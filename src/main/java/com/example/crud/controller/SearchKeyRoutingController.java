package com.example.crud.controller;

import com.example.crud.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("/demo/searchKeyRoutings")
    public ResponseEntity<?> getSearchKeyRoutings() {
        return ResponseEntity.ok(searchKeyRoutingService.getAll());
    }

    @GetMapping("/demo/searchKeyRouting/{id}")
    public ResponseEntity<?> findSearchKeyRoutingByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyRoutingService.findByID(id));
    }

    @DeleteMapping("/demo/deleteSearchKeyRouting/{id}")
    public void deleteSearchKeyRoutingByID(@PathVariable long id){
        searchKeyRoutingService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateSearchKeyRouting/{id}/{tableName}", method = RequestMethod.GET)
    public void updateSearchKeyRoutingByID(@PathVariable("id") long id, @PathVariable("tableName") String tableName){
        searchKeyRoutingService.updateByID(id, tableName);
    }
}
