package com.example.crud.controller;

import com.example.crud.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("/demo/nomenclatures")
    public ResponseEntity<?> getNomenclatures() {
        return ResponseEntity.ok(nomenclatureService.getAll());
    }

    @GetMapping("/demo/nomenclature/{id}")
    public ResponseEntity<?> findNomenclatureByID(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureService.findByID(id));
    }

    @DeleteMapping("/demo/deleteNomenclature/{id}")
    public void deleteNomenclatureByID(@PathVariable long id){
        nomenclatureService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateNomenclature/{id}/{year}", method = RequestMethod.GET)
    public void updateNomenclatureByID(@PathVariable("id") long id, @PathVariable("year") int year){
        nomenclatureService.updateByID(id, year);
    }
}
