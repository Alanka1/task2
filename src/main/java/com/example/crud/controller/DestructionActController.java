package com.example.crud.controller;

import com.example.crud.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestructionActController {
    private final DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }

    @GetMapping("/demo/destructionActs")
    public ResponseEntity<?> getDestructionActs() {
        return ResponseEntity.ok(destructionActService.getAll());
    }

    @GetMapping("/demo/destructionAct/{id}")
    public ResponseEntity<?> findDestructionActByID(@PathVariable long id){
        return ResponseEntity.ok(destructionActService.findByID(id));
    }

    @DeleteMapping("/demo/deleteDestructionAct/{id}")
    public void deleteDestructionActByID(@PathVariable long id){
        destructionActService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateDestructionAct/{id}/{base}", method = RequestMethod.GET)
    public void updateDestructionActByID(@PathVariable("id") long id, @PathVariable("base") String base){
        destructionActService.updateByID(id, base);
    }
}
