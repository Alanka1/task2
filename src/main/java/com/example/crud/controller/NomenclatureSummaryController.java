package com.example.crud.controller;



import com.example.crud.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }

    @GetMapping("/demo/nomenclatureSummaries")
    public ResponseEntity<?> getNomenclatureSummaries() {
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }

    @GetMapping("/demo/nomenclatureSummary/{id}")
    public ResponseEntity<?> findNomenclatureSummaryByID(@PathVariable long id){
        return ResponseEntity.ok(nomenclatureSummaryService.findByID(id));
    }

    @DeleteMapping("/demo/deleteNomenclatureSummary/{id}")
    public void deleteNomenclatureSummaryByID(@PathVariable long id){
        nomenclatureSummaryService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateNomenclatureSummary/{id}/{year}", method = RequestMethod.GET)
    public void updateNomenclatureSummaryByID(@PathVariable("id") long id, @PathVariable("year") int year){
        nomenclatureSummaryService.updateByID(id, year);
    }
}
