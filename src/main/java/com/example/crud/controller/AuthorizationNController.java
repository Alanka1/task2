package com.example.crud.controller;


import com.example.crud.service.AuthorizationNService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationNController {
    private final AuthorizationNService authorizationNService;

    public AuthorizationNController(AuthorizationNService authorizationNService) {
        this.authorizationNService = authorizationNService;
    }

    @GetMapping("/demo/authorizations")
    public ResponseEntity<?> getActivityJournals(){
        return ResponseEntity.ok(authorizationNService.getAll());
    }

    @GetMapping("/demo/authorization/{id}")
    public ResponseEntity<?> findAuthorizationByID(@PathVariable long id){
        return ResponseEntity.ok(authorizationNService.findByID(id));
    }

    @DeleteMapping("/demo/deleteAuthorization/{id}")
    public void deleteAuthorizationByID(@PathVariable long id){
        authorizationNService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateAuthorization/{id}/{username}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("username") String username){
        authorizationNService.updateByID(id, username);
    }
}

