package com.example.crud.controller;

import com.example.crud.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/demo/locations")
    public ResponseEntity<?> getLocations() {
        return ResponseEntity.ok(locationService.getAll());
    }

    @GetMapping("/demo/location/{id}")
    public ResponseEntity<?> findLocationByID(@PathVariable long id){
        return ResponseEntity.ok(locationService.findByID(id));
    }

    @DeleteMapping("/demo/deleteLocation/{id}")
    public void deleteLocationByID(@PathVariable long id){
        locationService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateLocation/{id}/{box}", method = RequestMethod.GET)
    public void updateLocationByID(@PathVariable("id") long id, @PathVariable("box") String box){
        locationService.updateByID(id, box);
    }
}
