package com.example.crud.controller;

import com.example.crud.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingController {
    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("/demo/fileRoutingServices")
    public ResponseEntity<?> getFileRoutingServices() {
        return ResponseEntity.ok(fileRoutingService.getAll());
    }

    @GetMapping("/demo/fileRoutingService/{id}")
    public ResponseEntity<?> findFileRoutingServiceByID(@PathVariable long id){
        return ResponseEntity.ok(fileRoutingService.findByID(id));
    }

    @DeleteMapping("/demo/deleteFileRoutingService/{id}")
    public void deleteFileRoutingServiceByID(@PathVariable long id){
        fileRoutingService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateFileRoutingService/{id}/{tableName}", method = RequestMethod.GET)
    public void updateFileRoutingServiceByID(@PathVariable("id") long id, @PathVariable("tableName") String tableName){
        fileRoutingService.updateByID(id, tableName);
    }
}
