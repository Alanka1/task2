package com.example.crud.controller;

import com.example.crud.service.CaseEService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseEController {
    private final CaseEService caseEService;

    public CaseEController(CaseEService caseEService) {
        this.caseEService = caseEService;
    }

    @GetMapping("/demo/cases")
    public ResponseEntity<?> getCases() {
        return ResponseEntity.ok(caseEService.getAll());
    }

    @GetMapping("/demo/case/{id}")
    public ResponseEntity<?> findCaseByID(@PathVariable long id){
        return ResponseEntity.ok(caseEService.findByID(id));
    }

    @DeleteMapping("/demo/deleteCase/{id}")
    public void deleteCaseByID(@PathVariable long id){
        caseEService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateCase/{id}/{caseHeadingEN}", method = RequestMethod.GET)
    public void updateCaseByID(@PathVariable("id") long id, @PathVariable("caseHeadingEN") String caseHeadingEN){
        caseEService.updateByID(id, caseHeadingEN);
    }
}
