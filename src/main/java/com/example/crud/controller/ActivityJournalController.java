package com.example.crud.controller;

import com.example.crud.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }

    @GetMapping("/demo/activityJournals")
    public ResponseEntity<?> getActivityJournals(){
        return activityJournalService.getActivityJournals();
    }

    @GetMapping("/demo/activityJournal/{id}")
    public ResponseEntity<?> findActivityJournalByID(@PathVariable long id){
        return activityJournalService.findActivityJournalByID(id);
    }

    @DeleteMapping("/demo/deleteActivityJournal/{id}")
    public void deleteActivityJournalByID(@PathVariable long id){
        activityJournalService.deleteActivityJournalByID(id);
    }

    @RequestMapping(value = "/demo/updateActivityJournal/{id}/{eventType}", method = RequestMethod.GET)
    public void updateActivityJournalByID(@PathVariable("id") long id, @PathVariable("eventType") String eventType){
        activityJournalService.updateActivityJournalByID(id,eventType);
    }

}
