package com.example.crud.controller;

import com.example.crud.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping("/demo/searchKeys")
    public ResponseEntity<?> getSearchKeys() {
        return ResponseEntity.ok(searchKeyService.getAll());
    }

    @GetMapping("/demo/searchKey/{id}")
    public ResponseEntity<?> findSearchKeyByID(@PathVariable long id){
        return ResponseEntity.ok(searchKeyService.findByID(id));
    }

    @DeleteMapping("/demo/deleteSearchKey/{id}")
    public void deleteSearchKeyByID(@PathVariable long id){
        searchKeyService.deleteByID(id);
    }

    @RequestMapping(value = "/demo/updateSearchKey/{id}/{name}", method = RequestMethod.GET)
    public void updateSearchKeyByID(@PathVariable("id") long id, @PathVariable("name") String name){
        searchKeyService.updateByID(id, name);
    }
}
