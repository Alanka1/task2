package com.example.crud.service;

import com.example.crud.repository.SearchKeyRoutingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SearchKeyRoutingService {
    private final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(searchKeyRoutingRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(searchKeyRoutingRepository.findById(id));
    }

    public void deleteByID(long id){
        searchKeyRoutingRepository.deleteById(id);
    }

    public void updateByID(long id, String tableName){
        searchKeyRoutingRepository.updateTableNameByID(tableName, id);
    }
}
