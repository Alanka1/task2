package com.example.crud.service;

import com.example.crud.repository.DestructionActRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class DestructionActService {
    private final DestructionActRepository destructionActRepository;

    public DestructionActService(DestructionActRepository destructionActRepository) {
        this.destructionActRepository = destructionActRepository;
    }

    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(destructionActRepository.findAll());
    }

    public ResponseEntity<?> findByID(long id){
        return ResponseEntity.ok(destructionActRepository.findById(id));
    }

    public void deleteByID(long id){
        destructionActRepository.deleteById(id);
    }

    public void updateByID(long id, String base){
        destructionActRepository.updateBaseByID(base, id);
    }
}
