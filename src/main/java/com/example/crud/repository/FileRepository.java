package com.example.crud.repository;
import com.example.crud.model.File;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface FileRepository extends CrudRepository<File, Long> {
    @Modifying
    @Transactional
    @Query("update File c set c.name =:name where c.id =:ID")
    public void updateNameByID(@Param("name") String name, @Param("ID") long id);
}
